const fs = require('fs');
const sa = require('superagent');

const JSON_FILES = [
    'backdrops.json',
    'costumes.json',
    'sounds.json'
];

JSON_FILES.forEach(fileName => {
    let filePath = './src/lib/libraries/' + fileName;
    // eslint-disable-next-line no-console
    console.log(__dirname, filePath);
    let dataArr = JSON.parse(fs.readFileSync(filePath));
    dataArr.forEach(data => {
        const assetName = data.md5ext;
        if (assetName) {
            const url = `https://cdn.assets.scratch.mit.edu/internalapi/asset/${assetName}/get/`;
            const savePath = './static/assets/' + assetName;
            if (!fs.existsSync(savePath)) {
                sa.get(url).pipe(fs.createWriteStream(savePath));
            }
        } else {
            // eslint-disable-next-line no-console
            console.log(data.name);
        }
    });
});
